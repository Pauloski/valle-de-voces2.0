const gulp = require('gulp');
const sass = require('gulp-sass');
const minify = require('gulp-minify');
const browserSync = require('browser-sync').create();

//compile scss into css
function style() {
  // where is my scss file
  return (
    gulp
      .src('./src/scss/**/*.scss')
      //compress
      .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
      //where do i save the compile css
      .pipe(gulp.dest('./dist/css'))
      //stream changes to all browser
      .pipe(browserSync.stream())
  );
}

function copyIndex() {
  return gulp.src('./src/index.html').pipe(gulp.dest('./dist/'));
}

function minifyJs() {
  return gulp
    .src('./src/js/**/*.js')
    .pipe(
      minify({
        ext: {
          min: '.min.js',
        },
      }),
    )
    .pipe(gulp.dest('./dist/js/'));
}

exports.default = function () {
  gulp.watch('./src/scss/**/*.scss', { ignoreInitial: false }, style);
  gulp.watch('./src/js/**/*.js', { ignoreInitial: false }, minifyJs);
  gulp.watch('./src/index.html', { ignoreInitial: false }, copyIndex);

  browserSync.init({
    server: {
      baseDir: './dist',
    },
  });
};
