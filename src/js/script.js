// Recoge el listado JSON e imprime función drawMiniCard(dato)
function fetchF() {
  var url = 'data.json';
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      prefetchImgs(data);
      drawMiniCards(data);
      showInitialModal();
    });
}

function prefetchImgs(data) {
  var imgs = [];
  Object.keys(data).forEach((key) => {
    var img = new Image();
    img.src =
      'https://www.valledevoces.cl/images/thumbnails/' + data[key].id + '.jpg';
    imgs.push(img);
  });
  for (var i = 0; i < 24; i++) {
    var img = new Image();
    img.src = '/images/bkg' + i + '.jpg';
    imgs.push(img);
  }
}

// Funcion imprime
function drawMiniCards(data) {
  let grid = '';
  for (let i in data) {
    grid +=
      "<div data-key='" +
      i +
      "' class='card' onclick='drawModal(" +
      data[i].id +
      ', "' +
      data[i].location +
      '" , "' +
      data[i].commune +
      '")\'>' +
      "<div class='card__image'>" +
      "<img src='https://www.valledevoces.cl/images/thumbnails/" +
      data[i].id +
      ".jpg'  />" +
      '</div>' +
      '</div>';
  }
  document.querySelector('.main-content').innerHTML += grid;
}

// Funcion modal
function drawModal(id, location, commune) {
  var newModal = document.createElement('div');
  newModal.setAttribute('id', 'modal');
  newModal.className = 'modal modal--show ';
  newModal.innerHTML =
    "<span id='modal__button-close' class='modal__close' onclick='closeModal(this.id, close)'> </span>" +
    "<span id='wavePlay-icon' class='wave-play'></span>" +
    "<div class='modal__content'>" +
    "<div id='waveform' class='modal__waveform'>" +
    "<div class='modal__info'>" +
    "<span class='modal__location'>" +
    location +
    '</span> -' +
    "<span class='modal__commune'> " +
    commune +
    '</span>' +
    '</div>' +
    '</div>' +
    "<img id ='imgModal' src='https://www.valledevoces.cl/images/modalimage/" +
    id +
    ".jpg' alt='nombre' />" +
    '</div>';
  var newBkgModal = document.createElement('div');
  newBkgModal.className = 'bkg-modal bkg-modal--show';
  document.getElementById('content').appendChild(newModal);
  document.getElementById('content').appendChild(newBkgModal);
  newModal.classList.remove('modal--hide');
  newModal.classList.add('modal--show');
  newBkgModal.classList.remove('bkgModal--hide');
  newBkgModal.classList.add('bkgModal--show');
  waveAudio(id);
}


function showInitialModal() {
  const modal = document.querySelector('.open-modal');
  modal.classList.remove('open-modal--init');
}

function closeInitialModal() {
  var x = document.getElementById('open-modal');
  x.classList.add('open-modal--out');
  x.parentNode.removeChild(x);
  appendBgItems();
  document
    .querySelector('.main__title')
    .classList.remove('main__title--hidden');
  document
    .querySelector('.main-content')
    .classList.remove('main-content--hidden');
  document.querySelector('.cb-slideshow').classList.remove('slides--hidden');
}

function appendBgItems() {
  let bg = '';
  for (var i = 0; i < 24; i++) {
    bg += '<li><span></span></li>';
  }
  document.querySelector('.cb-slideshow').innerHTML += bg;
}

function closeModal(clicked_id) {
  var x = document.getElementById(clicked_id).parentNode;
  var y = x.nextSibling;
  x.classList.remove('modal--show');
  x.classList.add('modal--hide');
  y.classList.remove('bkgModal--show');
  y.classList.add('bkgModal--hide');
  x.parentNode.removeChild(x);
  y.remove();
}



document.getElementById('trigger').addEventListener('click', function () {
  document.querySelector('.target').classList.toggle('max');
});

function waveAudio(id) {
  var wavesurfer = WaveSurfer.create({
    container: '#waveform',
    waveColor: '#cccccc',
    progressColor: '#e05b4d',
    barHeight: 2,
    height: 85,
  });
  wavesurfer.load('../media/' + id + '.mp3');

  wavesurfer.on('ready', function () {
    var playAudio = document.getElementById('wavePlay-icon');
    playAudio.addEventListener('click', function () {
      wavesurfer.play();
      console.log('../media/' + id + '.mp3');
      playAudio.remove();
    });
  });

  var stopAudio = document.getElementById('modal__button-close');
  stopAudio.addEventListener('click', function (e) {
    e.stopPropagation();
    wavesurfer.stop();
    wavesurfer.destroy();
  });
}

fetchF();
